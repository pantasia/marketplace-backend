FROM python:3.10-slim AS build
SHELL ["/bin/bash", "-o", "pipefail", "-o", "errexit", "-o", "nounset", "-o", "xtrace", "-c"]

RUN apt-get update && \
        apt-get install --assume-yes --no-install-recommends \
        # Needed to build python packages
        gcc \
        libc-dev \
        # Needed for psycopg
        libpq-dev && \
        rm --recursive --force /var/lib/apt/lists/*

COPY pyproject.toml poetry.lock ./

RUN python -m venv /pip_venv && \
    python -m venv /venv && \
    source /venv/bin/activate && \
    /pip_venv/bin/pip install --progress-bar=off poetry && \
    /pip_venv/bin/poetry install --no-dev --no-interaction

FROM python:3.10-slim AS production
SHELL ["/bin/bash", "-o", "pipefail", "-o", "errexit", "-o", "nounset", "-o", "xtrace", "-c"]

RUN apt-get update && \
    apt-get install --assume-yes --no-install-recommends \
    # Corresponding runtime libraries for the *-dev packages in build
    libpcre3 \
    libpq5 && \
    rm --recursive --force /var/lib/apt/lists/* && \
    useradd --create-home --shell /bin/bash pantasia

# Has to be same path as where we're copying from
COPY --from=build /venv /venv
ENV PATH="/venv/bin:$PATH"

ENV DJANGO_SETTINGS_MODULE=marketplace_backend.settings
ENV PYTHONPATH=/home/pantasia/code/src

USER pantasia
WORKDIR /home/pantasia/code
COPY --chown=pantasia:pantasia . ./

# stores the builds commit hash in an environment variable to be used in sentry
ARG COMMIT_HASH
ENV COMMIT_HASH=$COMMIT_HASH

FROM production AS development
SHELL ["/bin/bash", "-o", "pipefail", "-o", "errexit", "-o", "nounset", "-o", "xtrace", "-c"]

COPY --from=build /pip_venv /pip_venv
USER root
RUN apt update ; \
    apt install --assume-yes --no-install-recommends \
        # Required to upload test results
        curl ; \
    rm --recursive --force /var/lib/apt/lists/* ; \
    source /venv/bin/activate ; \
    /pip_venv/bin/poetry install --no-interaction --no-root
USER pantasia
