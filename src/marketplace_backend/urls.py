"""marketplace_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from strawberry_django_jwt.views import StatusHandlingGraphQLView as GQLView

from marketplace_backend.core.views import health
from marketplace_backend.graphql_api.api import schema

urlpatterns = [
    path("health/", health),  # type: ignore[arg-type]
    path("graphql/", csrf_exempt(GQLView.as_view(schema=schema))),
    # put this behind flag, expose for internal users only
    path("admin/", admin.site.urls),
]
