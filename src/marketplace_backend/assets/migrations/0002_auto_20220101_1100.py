# Generated by Django 3.2.9 on 2022-01-01 11:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("assets", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="royalty_addr",
            field=models.TextField(
                blank=True, null=True, verbose_name="receiving address for royalty"
            ),
        ),
        migrations.AddField(
            model_name="project",
            name="royalty_amount",
            field=models.FloatField(
                blank=True, null=True, verbose_name="royalty fraction"
            ),
        ),
        migrations.CreateModel(
            name="Trait",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.TextField(verbose_name="trait name")),
                ("value", models.TextField(verbose_name="trait value")),
                ("count", models.IntegerField(verbose_name="trait count")),
                ("percentage", models.FloatField(verbose_name="trait percentage")),
                (
                    "asset",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="traits",
                        to="assets.asset",
                    ),
                ),
            ],
        ),
    ]
