from typing import Optional

from django.db import connections
from graphql import GraphQLError

from marketplace_backend.assets.models import Asset, Project
from marketplace_backend.graphql_api.assets.types.assets import BlockchainAsset
from marketplace_backend.utils.db import dictfetchall


def get_blockchain_asset_by_id(asset_id: str) -> BlockchainAsset:
    query = """
    SELECT DISTINCT ON (ma_tx_mint.policy, ma_tx_mint.name)
    ENCODE("Asset"."assetId", 'hex') AS asset_id
    , ENCODE("Asset"."policyId", 'hex') AS policy_id
    , ENCODE("Asset"."assetName", 'escape') as asset_name
    , tx_metadata.json AS asset_metadata
    FROM "Asset"
    INNER JOIN
        ma_tx_mint ON ma_tx_mint.name = "Asset"."assetName"
        AND ma_tx_mint."policy" = "Asset"."policyId"
    INNER JOIN tx_metadata ON
        ma_tx_mint.tx_id = tx_metadata.tx_id
    WHERE
    "assetId" = decode(%s, 'hex')
    """

    with connections["cardano_db"].cursor() as cursor:
        cursor.execute(query, [asset_id])
        rows = dictfetchall(cursor)

    if rows is None:
        raise GraphQLError("Asset is not found in blockchain")
    assert len(rows) == 1
    return BlockchainAsset(**rows[0])


def get_or_create_asset(asset_id: str) -> Asset:
    """
    Get asset, and create asset if not exist
    """
    try:
        asset = Asset.objects.get(asset_id=asset_id)
    except Asset.DoesNotExist:
        # Cardano DB query
        blockchain_asset: BlockchainAsset = get_blockchain_asset_by_id(
            asset_id=asset_id
        )
        if blockchain_asset is None:
            raise GraphQLError("Asset is not found in blockchain")

        # Create Project if does not exist
        project, created = Project.objects.get_or_create(
            policy_id=blockchain_asset.policy_id
        )

        asset = Asset.objects.create(
            project=project,
            asset_id=blockchain_asset.asset_id,
            asset_name=blockchain_asset.asset_name,
            asset_verbose_name=blockchain_asset.verbose_name,
            ipfs_path=blockchain_asset.image_src,
            pinata_path=blockchain_asset.fast_image_src,
            metadata=blockchain_asset.metadata,
        )
    return asset


def get_project_details(policy_id: str) -> Optional[Project]:
    project: Optional[Project] = None
    try:
        project = Project.objects.get(policy_id=policy_id)
    except Project.DoesNotExist:
        pass
    return project
