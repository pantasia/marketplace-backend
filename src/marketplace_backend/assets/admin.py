from django.contrib import admin

from marketplace_backend.assets.models import Project


# https://github.com/typeddjango/django-stubs/issues/507
# the alternative is monkeypatch
class ProjectAdmin(admin.ModelAdmin):  # type: ignore[type-arg]
    list_display = (
        "name",
        "policy_id",
        "is_verified",
        "is_featured",
    )
    list_display_links = (
        "name",
        "policy_id",
    )
    list_filter = (
        "is_verified",
        "is_featured",
    )
    search_fields = (
        "name",
        "policy_id",
    )


admin.site.register(Project, ProjectAdmin)
