from django.contrib.auth import get_user_model
from django.core import signing

from marketplace_backend.accounts.models import User


def create_user(email: str, password: str) -> User:
    user_model = get_user_model()
    user: User = user_model.objects.create_user(  # type: ignore[has-type]
        email, password
    )
    return user


def get_verification_code(root: User) -> str:
    code = signing.dumps(root.email)
    return code


def verify_code(user: User, code: str) -> bool:
    try:
        decrypted_code: str = signing.loads(code)
    except signing.BadSignature:
        return False
    return user.email == decrypted_code


def verify_user(user: User) -> None:
    user.is_verified = True
    user.save()


def send_verification_email() -> None:
    """
    Send Verification Email via Sendgrid API
    """
    pass
