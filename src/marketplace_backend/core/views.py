from django.http.request import HttpRequest
from django.http.response import HttpResponse


async def health(request: HttpRequest) -> HttpResponse:
    return HttpResponse()
