from django.utils.crypto import get_random_string


def get_unique_address() -> str:
    """
    Send request to blockchain service to get unique address
    """

    return f"addr{get_random_string(length=32)}"
