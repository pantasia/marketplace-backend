import json
import logging
from typing import Any, Dict, List, Optional, Union

import strawberry
from pydantic import BaseModel, validator
from strawberry.django import auto

from marketplace_backend.assets.models import Asset as AssetModel
from marketplace_backend.graphql_api.assets.types.projects import Project
from marketplace_backend.graphql_api.types import JSONScalar

logger = logging.getLogger(__name__)


@strawberry.django.type(AssetModel)
class Asset:
    project: Project
    asset_id: auto
    asset_name: auto
    ipfs_path: auto
    pinata_path: auto
    metadata: JSONScalar


class BlockchainAsset(BaseModel):
    asset_id: str
    policy_id: str
    asset_name: str
    quantity: int = 1
    # String metadata from SQL query
    asset_metadata: str
    # Json Parsed Raw Metadata
    raw_metadata: Optional[Dict[Any, Any]] = None

    @validator("raw_metadata", pre=True, always=True)
    def default_metadata(
        cls, v: Optional[str], values: Dict[str, Any]
    ) -> Union[str, Dict[Any, Any]]:
        return v or json.loads(values["asset_metadata"])

    @property
    def verbose_name(self) -> str:
        try:
            data = self.raw_metadata[self.policy_id][self.asset_name]  # type: ignore
            if "name" in data:
                verbose_name: str = data["name"]
                return verbose_name
            # Here we can add extra custom policy to handle edge cases
        except Exception:
            logger.exception(
                f"Name does not exist in metadata, Asset Name : {self.asset_name}"
            )

        return self.asset_name

    @property
    def image_src(self) -> Optional[Union[str, List[str]]]:
        try:
            data = self.raw_metadata[self.policy_id][self.asset_name]  # type: ignore
            if "image" in data:
                image_src: Union[str, List[str]] = data["image"]
                return image_src
            # Here we can add extra custom policy to handle edge cases
            return None
        except Exception:
            logger.exception(
                f"Image does not exist in metadata, Asset Name : {self.asset_name}"
            )
            return None

    @property
    def fast_image_src(self) -> Optional[str]:
        image = self.image_src

        # Handling base64 encoded img
        if isinstance(image, list):
            return "".join(image)

        if isinstance(image, str):
            # URL with ipfs
            if "ipfs" in image:
                cid = image.replace("ipfs://", "")
            # Image with just IPFS CID
            else:
                cid = image
            return f"https://pantasia.mypinata.cloud/ipfs/{cid}"

        logging.error(f"Unseen Image Format, Asset Name : {self.asset_name}")
        return None

    @property
    def metadata(self) -> Optional[Dict[Any, Any]]:
        try:
            data = self.raw_metadata[self.policy_id][self.asset_name]  # type: ignore
        except Exception:
            logger.exception(f"Metadata is not in normal form : {self.asset_name}")
            # Here we can add extra custom policy to handle edge cases
            return self.raw_metadata

        # Stripping common keys by CIP25
        # https://cips.cardano.org/cips/cip25/
        data.pop("name", None)
        data.pop("files", None)
        data.pop("image", None)
        data.pop("mediaType", None)
        data.pop("description", None)
        return data  # type: ignore


class BlockchainAssets(BaseModel):
    assets: List[BlockchainAsset]
