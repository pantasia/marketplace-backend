import strawberry.django
from strawberry.django import auto

from marketplace_backend.assets.models import Project as ProjectModel


@strawberry.django.input(ProjectModel)
class ProjectCreateInput:
    policy_id: auto
    name: auto
    is_featured: auto
    is_verified: auto
