from typing import Any

from django.contrib.auth.models import AnonymousUser
from strawberry.permission import BasePermission

from marketplace_backend.accounts.models import User
from marketplace_backend.graphql_api.types import Info


class IsAuthenticated(BasePermission):
    message = "User is not authenticated"

    def has_permission(self, source: Any, info: Info, **kwargs: Any) -> bool:
        user = info.context.request.user
        assert isinstance(user, User) or isinstance(user, AnonymousUser)
        return user.is_authenticated
