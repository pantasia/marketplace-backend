import subprocess

from marketplace_backend.accounts.models import User as UserModel
from marketplace_backend.graphql_api.types import Info
from marketplace_backend.graphql_api.wallets.types.wallets import Wallet
from marketplace_backend.wallets.models import Wallet as WalletModel


class WalletCreationError(Exception):
    ...


def create_wallet(
    root: "WalletMutations",  # type: ignore[name-defined]
    info: Info,
    cbor_address: str,
    is_verified: bool,
) -> Wallet:
    user = info.context.request.user
    assert isinstance(user, UserModel)

    wallet_cbor_to_bech32_process = subprocess.run(
        f"echo {cbor_address} | ./bech32 addr", shell=True, capture_output=True
    )

    if wallet_cbor_to_bech32_process.stderr:
        raise WalletCreationError()

    bech32_wallet_address = wallet_cbor_to_bech32_process.stdout.rstrip().decode()

    # TODO: double check if this is always true
    # last 56 character
    cbor_stake_address = cbor_address[-56:]

    stake_prefix = "e1"  # e1 for mainnet, e0 for testnet
    stake_cbor_to_bech32_process = subprocess.run(
        f"echo {stake_prefix}{cbor_stake_address} | ./bech32 stake",
        shell=True,
        capture_output=True,
    )

    if stake_cbor_to_bech32_process.stderr:
        raise WalletCreationError()

    bech32_stake_address = stake_cbor_to_bech32_process.stdout.rstrip().decode()

    wallet_instance = WalletModel.objects.create(
        user=user,
        wallet_address=bech32_wallet_address,
        stake_address=bech32_stake_address,
        is_verified=is_verified,
    )
    return Wallet(  # type: ignore[call-arg]
        user=user,
        wallet_address=wallet_instance.wallet_address,
        stake_address=wallet_instance.stake_address,
        is_verified=wallet_instance.is_verified,
    )
