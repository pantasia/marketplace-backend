import strawberry
import strawberry.django

from marketplace_backend.graphql_api.permissions import IsAuthenticated
from marketplace_backend.graphql_api.wallets.mutations import create_wallet
from marketplace_backend.graphql_api.wallets.resolvers import (
    resolve_asset,
    resolve_assets,
)
from marketplace_backend.graphql_api.wallets.types.wallets import WalletAsset


@strawberry.type
class WalletQueries:
    assets: list[WalletAsset] = strawberry.field(
        resolver=resolve_assets, permission_classes=[IsAuthenticated]
    )
    asset: WalletAsset = strawberry.field(
        resolver=resolve_asset, permission_classes=[IsAuthenticated]
    )


@strawberry.type
class WalletMutations:
    wallet_create = strawberry.mutation(
        resolver=create_wallet, permission_classes=[IsAuthenticated]
    )
