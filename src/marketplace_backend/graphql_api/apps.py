from django.apps import AppConfig


class GraphqlApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "marketplace_backend.graphql_api"
