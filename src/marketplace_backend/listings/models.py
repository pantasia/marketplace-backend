from typing import Optional

from django.db import models
from django.utils.translation import gettext_lazy as _

from marketplace_backend import settings
from marketplace_backend.assets.models import Asset
from marketplace_backend.wallets.models import Wallet


class Listing(models.Model):
    class ListingType(models.TextChoices):
        FIXED_PRICE = "FIXED_PRICE", _("Fixed Price")
        OPEN_TO_BID = "OPEN_TO_BID", _("Open To Bid")
        AUCTION = "AUCTION", _("Auction")

    asset = models.ForeignKey(
        Asset,
        related_name="listings",
        on_delete=models.CASCADE,
    )
    listing_type = models.TextField(
        _("listing type"), choices=ListingType.choices, default=ListingType.FIXED_PRICE
    )
    owner_wallet_address = models.ForeignKey(
        Wallet,
        to_field="stake_address",
        related_name="listings",
        on_delete=models.CASCADE,
    )
    created_at = models.DateTimeField(_("listing created datetime"), auto_now_add=True)
    base_price_lovelace = models.IntegerField(
        _("base price in lovelace"), blank=True, null=True
    )
    starts_at = models.DateTimeField(_("listing start datetime"), auto_now_add=True)
    ends_at = models.DateTimeField(_("listing end datetime"), blank=True, null=True)
    is_active = models.BooleanField(_("is listing active"), default=False)

    @property
    def base_price_ada(self) -> Optional[float]:
        """
        Returns the base price in ADA
        """
        if self.base_price_lovelace is None:
            return None
        return self.base_price_lovelace / (10 ** 6)

    # TODO : add custom validator to reject null price when type is not OPEN_TO_BID


class Bid(models.Model):
    asset = models.ForeignKey(Asset, related_name="bids", on_delete=models.CASCADE)
    price_lovelace = models.IntegerField(_("bid price in lovelace"))
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="bids", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(_("bid created datetime"), auto_now_add=True)

    @property
    def price_ada(self) -> float:
        """
        Returns the price in ADA
        """
        return self.price_lovelace / (10 ** 6)


class Transaction(models.Model):
    bid = models.OneToOneField(Bid, on_delete=models.CASCADE)
    listing = models.OneToOneField(Listing, on_delete=models.CASCADE)
    created_at = models.DateTimeField(
        _("transaction created datetime"), auto_now_add=True
    )
    transaction_address = models.TextField(
        _("dedicated address for transaction"),
    )
    has_buyer_transfer = models.BooleanField(
        _("has buyer transfer crypto"), default=False
    )
    has_seller_transfer = models.BooleanField(
        _("has seller transfer NFT"), default=False
    )
    is_processed = models.BooleanField(_("has marketplace processed"), default=False)
    ttl_buyer_sec = models.IntegerField(_("time window for buyer to pay"), default=600)
    ttl_seller_sec = models.IntegerField(
        _("time window for seller to pay"), default=14400
    )
