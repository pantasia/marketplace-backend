from uvicorn.workers import UvicornWorker


class Worker(UvicornWorker):  # type: ignore[misc]
    CONFIG_KWARGS = {"loop": "uvloop", "http": "httptools", "lifespan": "off"}
