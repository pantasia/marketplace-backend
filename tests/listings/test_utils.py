from datetime import datetime, timezone

import pytest

from marketplace_backend.graphql_api.listings.types.listing import ListingType
from marketplace_backend.listings.models import Listing
from marketplace_backend.listings.utils import create_new_listing

from tests.helpers.factories import AssetFactory, ListingFactory, WalletFactory


class TestCreateNewListing:
    @pytest.mark.django_db
    def test_no_previous_listing(self):
        asset_id = "asset_id"
        asset = AssetFactory(asset_id=asset_id)
        wallet = WalletFactory()
        expiring_datetime = datetime.fromisoformat("2021-12-31").replace(
            tzinfo=timezone.utc
        )
        base_price_lovelace = 5000

        listing = create_new_listing(
            asset_id,
            asset,
            wallet,
            ListingType.FIXED_PRICE,
            base_price_lovelace,
            None,
            expiring_datetime,
        )

        # only 1 listing
        assert Listing.objects.get()
        assert listing.is_active is True
        assert str(listing.asset.asset_id) == asset_id
        assert listing.asset == asset
        assert listing.owner_wallet_address == wallet
        assert listing.base_price_lovelace == base_price_lovelace
        assert listing.ends_at == expiring_datetime

    @pytest.mark.django_db
    def test_with_existing_listing(self):
        asset_id = "asset_id"
        asset = AssetFactory(asset_id=asset_id)
        wallet = WalletFactory()
        expiring_datetime = datetime.fromisoformat("2021-12-31").replace(
            tzinfo=timezone.utc
        )
        base_price_lovelace = 5000

        existing_listing = ListingFactory(asset=asset, is_active=True)

        listing = create_new_listing(
            asset_id,
            asset,
            wallet,
            ListingType.FIXED_PRICE,
            base_price_lovelace,
            None,
            expiring_datetime,
        )

        existing_listing.refresh_from_db()
        assert not existing_listing.is_active

        assert Listing.objects.count() == 2
        assert listing.is_active is True
        assert str(listing.asset.asset_id) == asset_id
        assert listing.asset == asset
        assert listing.owner_wallet_address == wallet
        assert listing.base_price_lovelace == base_price_lovelace
        assert listing.ends_at == expiring_datetime
