import pytest
from django.test import Client


class JsonClient(Client):
    def post(self, *args, **kwargs):
        kwargs.setdefault("content_type", "application/json")
        return super().post(*args, **kwargs)


@pytest.fixture
def client():
    """replace the `client` fixture from `pytest-django` with the above,
    which defaults to "application/json" instead of "multipart/form-data"
    """
    return JsonClient()


@pytest.fixture(scope="session")
def django_db_modify_db_settings():
    from django.conf import settings

    # do not run test with cardano_db
    del settings.DATABASES["cardano_db"]
