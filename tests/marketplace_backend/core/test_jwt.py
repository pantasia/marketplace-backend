from datetime import datetime, timedelta, timezone
from unittest import mock

import pytest
import time_machine
from strawberry_django_jwt.object_types import TokenPayloadType

from marketplace_backend.core.jwt import (
    calculate_refresh_expires_in,
    create_access_token,
    create_jwt_payload,
)

from tests.helpers.factories import UserFactory


@pytest.mark.django_db
@mock.patch("marketplace_backend.core.jwt.jwt_settings")
def test_create_jwt_payload(mock_jwt_settings):
    def mock_create_payload(user):
        return TokenPayloadType(exp=0, origIat=0, email=user.email)

    mock_jwt_settings.JWT_PAYLOAD_HANDLER = mock_create_payload

    user = UserFactory()
    assert create_jwt_payload(user) == mock_create_payload(user)


@mock.patch("marketplace_backend.core.jwt.jwt_settings")
def test_create_access_token(mock_jwt_settings):
    def mock_encode_handler(_):
        return "foo"

    mock_jwt_settings.JWT_ENCODE_HANDLER = mock_encode_handler

    assert create_access_token(TokenPayloadType()) == "foo"


@mock.patch("marketplace_backend.core.jwt.jwt_settings")
def test_calculate_refresh_expires_in(mock_jwt_settings):
    time_delta = timedelta(days=1)
    mock_jwt_settings.JWT_REFRESH_EXPIRATION_DELTA = time_delta

    datetime_now = datetime(2021, 6, 6, 00, 0, 0, tzinfo=timezone.utc)
    with time_machine.travel(datetime_now, tick=False):
        refresh_expires_in = calculate_refresh_expires_in()

    assert refresh_expires_in == int((datetime_now + time_delta).timestamp())
