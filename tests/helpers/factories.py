from factory import Faker, SelfAttribute, SubFactory, fuzzy
from factory.django import DjangoModelFactory

from marketplace_backend.accounts import models
from marketplace_backend.assets.models import Asset, Project
from marketplace_backend.graphql_api.listings.types.listing import ListingType
from marketplace_backend.listings.models import Bid, Listing
from marketplace_backend.wallets.models import Wallet


class UserFactory(DjangoModelFactory):  # type: ignore[misc]
    class Meta:
        model = models.User

    email = Faker("email")
    username = SelfAttribute("email")
    display_name = Faker("name")
    is_verified = Faker("boolean")


class ProjectFactory(DjangoModelFactory):  # type: ignore[misc]
    class Meta:
        model = Project

    policy_id = Faker("uuid4")
    name = Faker("name")
    is_featured = Faker("boolean")
    is_verified = Faker("boolean")


class AssetFactory(DjangoModelFactory):  # type: ignore[misc]
    class Meta:
        model = Asset

    project = SubFactory(ProjectFactory)
    asset_id = Faker("uuid4")
    asset_name = Faker("name")
    asset_verbose_name = Faker("name")
    ipfs_path = Faker("image_url")
    pinata_path = Faker("image_url")
    metadata = Faker("json")


class WalletFactory(DjangoModelFactory):  # type: ignore[misc]
    class Meta:
        model = Wallet

    user = SubFactory(UserFactory)
    wallet_address = Faker("uuid4")
    stake_address = Faker("uuid4")
    is_verified = Faker("boolean")


class ListingFactory(DjangoModelFactory):  # type: ignore[misc]
    class Meta:
        model = Listing

    asset = SubFactory(AssetFactory)
    listing_type = fuzzy.FuzzyChoice(list(ListingType))
    owner_wallet_address = SubFactory(WalletFactory)
    base_price_lovelace = Faker("random_int")
    is_active = Faker("boolean")


class BidFactory(DjangoModelFactory):  # type: ignore[misc]
    class Meta:
        model = Bid

    asset = SubFactory(AssetFactory)
    price_lovelace = Faker("random_int")
    user = SubFactory(UserFactory)
