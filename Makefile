NAME=marketplace_backend

.PHONY: help
help: ## List and describe public targets
	@# Any targets with double-hashed comments will be treated as public, and those comments will be used as help text
	@grep -E '^[a-zA-Z_-]+:.*?##\s?.*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?##\s?"}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: dev-bootstrap
dev-bootstrap: build wakeup-database migrate ## Initial set up of development environment

.PHONY: build
build: ## Build marketplace_backend docker image
	docker-compose build $(NAME)

.PHONY: runserver
runserver: ## Run marketplace_backend service
	docker-compose run --service-ports --rm $(NAME)

.PHONY: migrate
migrate: ## Run django-admin migrate command
	docker-compose run --rm $(NAME) django-admin migrate

.PHONY: migrations
migrations: ## Run django-admin makemigrations command
	docker-compose run --rm $(NAME) django-admin makemigrations

.PHONY: shell
shell: ## Run django-admin shell command
	docker-compose run --rm $(NAME) django-admin shell

.PHONY: pytest
pytest: ## Run pytest
	docker-compose run --rm $(NAME) pytest $${TEST_ARGS:-}

.PHONY: wakeup-database
wakeup-database:
	docker-compose up -d db
